FROM php:7.2-apache


RUN apt update && \
    apt install -y libzip-dev && \ 
    apt install -y libonig-dev && \ 
    apt-get install -y libcurl4-openssl-dev && \ 
    apt-get install -y libicu-dev && \ 
    #apt-get install a2enmod ssl rewrite && \ 
    docker-php-ext-install pdo pdo_mysql zip intl curl && \ 
    pecl install xdebug && \ 
    echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/xdebug.ini && \
    echo "xdebug.remote_enable=on" >> /usr/local/etc/php/conf.d/xdebug.ini && \
    echo "xdebug.remote_autostart=on" >> /usr/local/etc/php/conf.d/xdebug.ini && \
    echo "xdebug.remote_connect_back=on" >> /usr/local/etc/php/conf.d/xdebug.ini && \
    echo "xdebug.idekey=vscode" >> /usr/local/etc/php/conf.d/xdebug.ini && \
    echo "xdebug.remote_log=/tmp/xdebug_remote_log" >> /usr/local/etc/php/conf.d/xdebug.ini

# Microsoft SQL Server Prerequisites
ENV ACCEPT_EULA=Y

RUN apt-get update \
    && apt-get install -y gnupg \
    && curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - \
    && curl https://packages.microsoft.com/config/debian/9/prod.list \
        > /etc/apt/sources.list.d/mssql-release.list \
    && apt-get install -y --no-install-recommends \
        locales \
        apt-transport-https \
    && echo "en_US.UTF-8 UTF-8" > /etc/locale.gen \
    && locale-gen \
    && apt-get update \
    && apt-get -y --no-install-recommends install \
        msodbcsql17 \
        unixodbc-dev

RUN docker-php-ext-install mbstring pdo pdo_mysql mysqli \
    && pecl install sqlsrv pdo_sqlsrv xdebug \
    && docker-php-ext-enable sqlsrv pdo_sqlsrv mysqli xdebug && \
    a2enmod rewrite && \
    echo 'xdebug.profiler_enable_trigger=1' >> /usr/local/etc/php/conf.d/php.ini && \
    echo 'xdebug.remote_enable=1' >> /usr/local/etc/php/conf.d/php.ini && \
    echo 'xdebug.remote_autostart=1' >> /usr/local/etc/php/conf.d/php.ini && \
    echo 'xdebug.remote_port=9000' >> /usr/local/etc/php/conf.d/php.ini && \
    echo 'xdebug.remote_connect_back=1' >> /usr/local/etc/php/conf.d/php.ini && \
    echo 'xdebug.remote_log=/tmp/xdebug_remote_log' >> /usr/local/etc/php/conf.d/php.ini && \
    echo 'session.save_path=/tmp' >> /usr/local/etc/php/conf.d/php.ini

COPY ./ /var/www/
COPY public /var/www/html
#COPY system /var/www/system
#COPY writable /var/www/writable

RUN mkdir -p /var/php_sessions && chown www-data:www-data /var/php_sessions

# Set the ownership of the PHP sessions folder AFTER it has been mounted to the host's file system,
# otherwise it will be root:root, which PHP does not like.

CMD touch /tmp/xdebug_remote_log && chown www-data:www-data /var/php_sessions /tmp/xdebug_remote_log && apache2-foreground


